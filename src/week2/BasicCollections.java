package week2;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class BasicCollections {
    public static void main(String[] args) {
        Integer[] values = new Integer[]{5, 6, 7, 8, 8, 4, 3, 2, 1, 4};

        System.out.println("-SET-");
        System.out.println("All set implementations don't hold duplicate values. It will keep the original value over a duplicate.");
        //HashSet
        System.out.println("---HASHSET---");

        System.out.println("Doesn't order the items in a specific way.");

        Set hSet = new HashSet();

        for (int i: values) {
            hSet.add(i);
        }

        hSet.add("apple");
        hSet.add(null);

        System.out.println(hSet + "\n");

        //LinkedHashSet
        System.out.println("---LINKEDHASHSET---");

        System.out.println("Maintains the order that elements were added.");

        Set linkHashSet = new LinkedHashSet();

        for (int i: values) {
            linkHashSet.add(i);
        }

        linkHashSet.add("apple");
        linkHashSet.add(null);

        System.out.println(linkHashSet + "\n");

        //TreeSet
        System.out.println("---TREESET---");

        System.out.println("Ordered in a specific order (either the default order or one supplied by user)");
        System.out.println("Cannot hold null or Heterogeneous objects");

        Set tSet = new TreeSet();

        for (Integer i: values) {
            tSet.add(i);
        }

        System.out.println(tSet + "\n");

        //list
        System.out.println("-LIST-");
        System.out.println("Stores elements in the order that they are added. Can hold duplicates.");
        System.out.println("ArrayList and LinkedList are functionally the same in simple situations but when dealing\n"+
                "with large amounts of data there are performance differences based on the desired action.");

        List list = new ArrayList();

        for (int i: values) {
            list.add(i);
        }

        list.add("apple");

        System.out.println(list + "\n");

        //queue
        System.out.println("-QUEUE-");
        System.out.println("Used by LinkedList and PriorityQueue. Uses methods that pull the first element off to be read.");

        //LinkedList Queue
        System.out.println("\n---LinkedList---");
        System.out.println("Orders the list based on the add order. Uses FIFO with the poll() method.");

        Queue queue = new LinkedList();

        for (int i: values) {
            queue.add(i);
        }

        while (queue.iterator().hasNext()) {
            System.out.print(queue.poll());
        }

        //PriorityQueue
        System.out.println("\n\n---PriorityQueue---");
        System.out.println("Orders elements based on the comparator, either default or provided to it.");
        System.out.println("Cannot hold null or Heterogeneous objects");

        Queue pQueue = new PriorityQueue();

        for (int i: values) {
            pQueue.add(i);
        }

        while (pQueue.iterator().hasNext()) {
            System.out.print(pQueue.poll());
        }

        //deque
        System.out.println("\n\n-DEQUE-");
        System.out.println("Operates similar to a queue but can add to either the beginning or the end.");

        Deque deque = new ArrayDeque();

        for (int i = 0; i < values.length; i++){
            deque.offerFirst(values[i]);
            i++;
            deque.offerLast(values[i]);
        }

        while (deque.iterator().hasNext()) {
            System.out.print(deque.poll());
        }

        //map
        System.out.println("\n\n-MAP-");
        System.out.println("Maps use keys to hold values. If a duplpicate key is input, it overrides the old value with the new one.");

        String[] key = new String[] {"Joe","joe","Frank","frank","Adam","adam","Jeniffer","jeniffer","Bob","bob"};

        //HashMap
        System.out.println("\n---HASHMAP---");
        System.out.println("Sorts the keys by their hash values. Allows a null key.");

        Map hMap = new HashMap();

        for (int i = 0; i < key.length; i++) {
            hMap.put(key[i], key[i]);
        }

        hMap.put(null, 0);
        hMap.put("Adam", "Changed");

        System.out.println(hMap);

        //LinkedHashMap
        System.out.println("\n---LINKEDHASHMAP---");
        System.out.println("Orders the keys by their added order. Allows a null key.");

        Map lHMap = new LinkedHashMap();

        for (int i = 0; i < key.length; i++) {
            lHMap.put(key[i], key[i]);
        }

        lHMap.put(null, 0);
        lHMap.put("Adam", "Changed");

        System.out.println(lHMap);

        //TreeMap
        System.out.println("\n---TREEMAP---");
        System.out.println("Orders the keys in a specific order. Doesn't allow null keys.");

        Map tMap = new TreeMap();

        for (int i = 0; i < key.length; i++) {
            tMap.put(key[i], key[i]);
        }

        tMap.put("Adam", "Changed");

        System.out.println(tMap);
    }
}
